import java.util.Scanner;

public class DemoBit2Dec {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String bitString = scanner.next();
        int result = convertBitToDec(bitString);
        if (result == 0) {
            System.out.println("Invalid number. Try again");
        } else {
            System.out.println(" The decimal number of " + bitString + " is " + result);
        }
    }

    public static int convertBitToDec(String bitString) {
        if (!isValid(bitString) || !isBinaryNumber(bitString)) {
            return 0;
        }
        int result = 0;
        int size = bitString.length();
        for (int i = size - 1; i >= 0; i--) {
            int currentInteger = Integer.parseInt(bitString.substring(0, 1));
            bitString = bitString.substring(1);
            result = result + currentInteger * (int) Math.pow(2, i);
        }
        return result;
    }

    public static boolean isValid(String bitString) {
        return bitString.length() < 8 && bitString != null;

    }

    public static boolean isBinaryNumber(String bitString) {
        boolean valid = false;
        for (int i = 0; i < bitString.length(); i++) {
            if (bitString.charAt(i) == 0 || bitString.charAt(i) == 1) {
                valid = true;
            } else {
                valid = false;
            }
        }
        return valid;
    }
}
